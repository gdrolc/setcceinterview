package com.ccasper89.springbootvuetodo;

public enum TaskPriority {
  LOW,
  NORMAL,
  IMPORTANT,
  CRITICAL;
}